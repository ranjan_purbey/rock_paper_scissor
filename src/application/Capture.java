package application;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Capture {
	VideoCapture cap;
	ImageView player;
	Capture(Main main){
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		this.cap=new VideoCapture(0);
		this.player=new ImageView();
		new Timer().schedule(new TimerTask() {
			
			@Override
			public void run() {
				Image img=null,contImg=null;
				if(cap.isOpened()){
					try{
						//Capture Image
						Mat frame=new Mat(),contFrame;
						cap.read(frame);
						contFrame=Mat.zeros(frame.size(), CvType.CV_32FC3);
						main.parsedMove=null;
						//if(this.grayScale.isSelected())
							Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2GRAY);
						//if(this.gBlur.isSelected())
							Imgproc.GaussianBlur(frame, frame, new Size(29,29), 0);
						//if(this.thresh.isSelected())
							Imgproc.threshold(frame, frame,160, 250, Imgproc.THRESH_BINARY);
						//if(this.contour.isSelected()){
							List<MatOfPoint> cont=new ArrayList<MatOfPoint>();
							Imgproc.findContours(frame, cont, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
							for(int i=0;i<cont.size();i++){
							//	if(Imgproc.contourArea(cont.get(i))>Imgproc.contourArea(cont.get(ci)))ci=i;
								if(Imgproc.contourArea(cont.get(i))>10000){
								MatOfInt hull=new MatOfInt();
								MatOfInt4 defects=new MatOfInt4();
								Imgproc.convexHull(cont.get(i), hull);
								int[] hullArray=hull.toArray();
								List<Point> hullPoints = new ArrayList<Point>();
						        hullPoints.clear();
						        for (int j = 0; j < hullArray.length; j++) 
						            hullPoints.add(cont.get(i).toList().get(hull.toList().get(j)));
						        MatOfPoint tmp=new MatOfPoint();
						        tmp.fromList(hullPoints);
								//Imgproc.drawContours(contFrame, Arrays.asList(tmp),0, new Scalar(50,250,0),1);
								Imgproc.convexityDefects(cont.get(i), hull, defects);
								int fingers=0;
								for(int j=0;j<defects.size().height;j++){
									if(defects.get(j,0)[3]/256>100)fingers++;
									Imgproc.line(contFrame, cont.get(i).toList().get((int)defects.get(j,0)[0]),cont.get(i).toList().get((int)defects.get(j,0)[1]),new Scalar(50,200,0),1);
									Imgproc.circle(contFrame, cont.get(i).toList().get((int)defects.get(j,0)[2]), 10, new Scalar(0,10,200),-1);
								}
//								System.out.println((fingers>2)?"Paper":(fingers==2?"Scissors":(fingers<2?"Rock":"Invalid")));
								main.parsedMove=(fingers>2)?Board.Moves.PAPER:(fingers==2?Board.Moves.SCISSORS:(fingers<2?Board.Moves.ROCK:null));
							}
							}
							
							Imgproc.drawContours(contFrame, cont, -1, new Scalar(255,255,255),-1);
							
							
						//}
						MatOfByte buffer=new MatOfByte();
						Imgcodecs.imencode(".png", frame, buffer);
						img=new Image(new ByteArrayInputStream(buffer.toArray()));
						MatOfByte contBuffer=new MatOfByte();
						Imgcodecs.imencode(".png", contFrame, contBuffer);
						contImg=new Image(new ByteArrayInputStream(contBuffer.toArray()),250,250,true,false);
					}
					catch(Exception e){System.err.println("ERROR:"+e);}
				}
				player.setImage(contImg);
			}
			
		},0,40);
		player.setX(main.w-250);
		player.setY(main.h-250);
//		player.setFitWidth(250);
//		player.setFitHeight(250);
	}
}

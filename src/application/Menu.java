package application;

import java.util.Timer;
import java.util.TimerTask;

import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class Menu{
	Text heading, play, instructions, license, exit;
	void render(Main main){
		main.root.getChildren().clear();
		heading=new Text(.25*main.w,.25*main.h,"Rock-Paper-Scissors");
		heading.setStyle("-fx-font:60 'Monospace'; -fx-stroke: rgba(0,180,250,.7)");
		play=new Text(.25*main.w-100,.75*main.h,"Play");
		play.setStyle("-fx-font:40px Monospace;-fx-fill:rgba(0,0,0,0);-fx-stroke: steelblue");
		instructions=new Text(.75*main.w-100, .75*main.h, "Instructions");
		instructions.setStyle("-fx-font:40px Monospace;-fx-fill:rgba(0,0,0,0);-fx-stroke: steelblue");
		license=new Text(.5*main.w-100, .9*main.h, "License");
		license.setStyle("-fx-font:40px Monospace;-fx-fill:rgba(0,0,0,0);-fx-stroke: steelblue");
		exit=new Text(20,main.h-10,"↫ Quit");
		exit.setStyle("-fx-stroke:red;-fx-font-size:25px");
		main.root.getChildren().addAll(heading,play,instructions,license, exit);
		main.say("Welcome User.","Do you want to play, or see instructions?");
		listen(main);
		
	}
	void listen(Main main){
		for(Text t:new Text[]{play,instructions,license,exit}){
			t.setOnMouseEntered((MouseEvent e)->{
				t.setFill((t!=exit)?Color.STEELBLUE:Color.RED);
			});
			t.setOnMouseExited((MouseEvent e)->{
				t.setFill(Color.TRANSPARENT);
			});
		}
		play.setOnMouseClicked((MouseEvent e)->{
			main.board.render(main);
		});
		instructions.setOnMouseClicked((MouseEvent e)->{
			main.manual.render(main);
		});
		license.setOnMouseClicked((MouseEvent e)->{
			main.legal.render(main);
		});
		exit.setOnMouseClicked((MouseEvent e)->{
			System.exit(0);
		});
	}
}

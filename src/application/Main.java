package application;

import java.util.Timer;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class Main extends Application {
	Canvas canvas;
	GraphicsContext ctx;
	float h,w;
	Voice voice;
	Menu menu;
	Manual manual;
	Board board;
	GameOver gameOver;
	Legal legal;
	Capture capture;
	Timer gameLoop;
	Pane root;
	Board.Moves parsedMove;
	@Override
	public void start(Stage stage) {
//		System.out.println(Arrays.toString(VoiceManager.getInstance().getVoices()));
		
		this.menu=new Menu();
		this.manual=new Manual();
		this.board=new Board();
		this.gameOver=new GameOver();
		this.legal=new Legal();
		this.voice = VoiceManager.getInstance().getVoice("kevin16");
//		this.voice.setAge(Age.CHILD);
		this.voice.allocate();
		this.voice.setStyle("breathy");
		this.voice.setRate(120);
		this.voice.setPitch(100);
		this.gameLoop=null;
		Rectangle2D bounds=Screen.getPrimary().getVisualBounds();
		this.h=(float)bounds.getHeight();
		this.w=(float)bounds.getWidth();
		this.canvas=new Canvas(w,h);
		this.ctx=canvas.getGraphicsContext2D();
		this.root=new Pane();
		this.root.setStyle("-fx-background-image: url('file:res/paper.png');");
		stage.setScene(new Scene(this.root,w,h));
		stage.getScene().getStylesheets().add(Main.class.getResource("application.css").toExternalForm());
		stage.setTitle("ROCK-PAPER-SCISSOR");
		stage.show();
		stage.setFullScreen(true);
		this.menu.render(this);
//		new Thread(new Runnable() {
//			
//			@Override
//			public void run() {
				Main.this.capture=new Capture(Main.this);
//			}
//		},"Capture").start();
		stage.setOnCloseRequest((WindowEvent e)->{
			System.exit(0);
		});
		
	 }
	void say(boolean wait,String... msg){
		Thread t=new Thread(new Runnable(){
			public void run(){

				for(String s:msg){
					voice.speak(s);
				}
			}
		},"Voice");
		t.start();
		if(wait)
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}
	void say(String... msg){say(false,msg);}
	public static void main(String[] args) {
		launch(args);
	}
}

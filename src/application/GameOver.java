package application;

import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class GameOver {
	Text gameOverLbl, winnerLbl, replayLbl, exitLbl;
	void conclude(Main main){
		main.root.getChildren().clear();
		gameOverLbl=new Text(.5*main.w-100,.25*main.h,"Game Over");
		gameOverLbl.setStyle("-fx-font:40px Monospace; -fx-stroke:red");
		winnerLbl=new Text(.5*main.w-200,.4*main.h,"Winner: "+(main.board.cpuScore>main.board.playerScore?"CPU":"Player"));
		winnerLbl.setStyle("-fx-font:60px Serif;-fx-stroke: darkorchid;");
		replayLbl=new Text(.25*main.w,.7*main.h,"↻ Play Again");
		exitLbl=new Text(.7*main.w,.7*main.h,"↩ Back To Menu");
		replayLbl.setStyle("-fx-font:30px Serif;");
		exitLbl.setStyle("-fx-font:30px Serif;");
		main.root.getChildren().addAll(gameOverLbl, winnerLbl, replayLbl, exitLbl);
		for(Text t:new Text[]{replayLbl,exitLbl}){
			t.setOnMouseEntered((MouseEvent e)->{
				t.setFill(Color.YELLOW);
			});
			t.setOnMouseExited((MouseEvent e)->{
				t.setFill(Color.TRANSPARENT);
			});
		}
		replayLbl.setOnMouseClicked((MouseEvent e)->{
			main.board.render(main);
		});
		exitLbl.setOnMouseClicked((MouseEvent e)->{
			main.menu.render(main);
		});
	}
}

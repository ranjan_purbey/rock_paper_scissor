package application;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class Board{
	int cpuScore,playerScore;
	Text cpuScoreLbl, cpuLbl, cpuMoveLbl, playerScoreLbl, playerLbl, playerMoveLbl, exitLbl, resultLbl;
	Moves playerMove, cpuMove;
	void render(Main main){
		cpuScore=5;
		playerScore=5;
		main.root.getChildren().clear();
		Line partition=new Line(.5*main.w, 100, .5*main.w, main.h-300);
		cpuLbl=new Text(.2*main.w,.2*main.h,"CPU");
		playerLbl=new Text(.7*main.w,.2*main.h,"Player");
		cpuLbl.setStyle("-fx-font:50px Monospace;-fx-stroke:blue;-fx-fill:rgba(0,0,0,0)");
		playerLbl.setStyle("-fx-font:50px Monospace;-fx-stroke:blue;-fx-fill:rgba(0,0,0,0)");
		cpuScoreLbl=new Text(.15*main.w,40,"Score: "+cpuScore);
		playerScoreLbl=new Text(.85*main.w,40,"Score: "+playerScore);
		cpuMoveLbl=new Text(.1*main.w,.5*main.h,"");
		playerMoveLbl=new Text(.55*main.w,.5*main.h,"");
		cpuMoveLbl.setStyle("-fx-font:70px Serif");
		playerMoveLbl.setStyle("-fx-font:70px Serif");
		exitLbl=new Text(100, main.h-200, "↩ Back To Menu");
		exitLbl.setOnMouseEntered((MouseEvent e)->{
			exitLbl.setFill(Color.ORANGERED);
		});
		exitLbl.setOnMouseExited((MouseEvent e)->{
			exitLbl.setFill(Color.TRANSPARENT);
		});
		exitLbl.setOnMouseClicked((MouseEvent e)->{
			main.gameLoop.cancel();
			main.menu.render(main);
		});
		resultLbl=new Text(main.w*.5-100, main.h-200,"");
		resultLbl.setStyle("-fx-fill:darkviolet");
		main.root.getChildren().addAll(partition,cpuLbl,playerLbl,cpuScoreLbl,playerScoreLbl, cpuMoveLbl, playerMoveLbl, exitLbl, resultLbl,main.capture.player);
		main.voice.setRate(150);
//		try{
//			while(true){
//				main.say(true, "Rock, Paper, Scissors");
//				Thread.sleep(100);
//			}
//		}
//		catch(Exception e){}
		main.gameLoop=new Timer();
		main.gameLoop.schedule(new TimerTask(){
			public void run(){
//				if(!firstRun){
//					cpuLbl.setText(i+++"");
//				}
//				else firstRun=false;
				cpuMoveLbl.setText("");
				playerMoveLbl.setText("");
				resultLbl.setText("");
				main.say(true,"Rock Paper Scissors");
				cpuMove=Moves.values()[new Random().nextInt(Moves.values().length)];
				cpuMoveLbl.setText(cpuMove.toString());
				playerMove=main.parsedMove;
				playerMoveLbl.setText(playerMove!=null?playerMove.toString():"INVALID MOVE");
				if(cpuMove!=playerMove){
					if((cpuMove==Moves.PAPER && playerMove!=Moves.SCISSORS) || (cpuMove==Moves.SCISSORS && playerMove!=Moves.ROCK)|| (cpuMove==Moves.ROCK && playerMove!= Moves.PAPER)){
						cpuScore++;playerScore--;
						resultLbl.setText("CPU +1 ; Player -1");
					}
					else {
						playerScore++;cpuScore--;
						resultLbl.setText("CPU -1 ; Player +1");
					}
				}
				else resultLbl.setText("Tie");
				playerScoreLbl.setText("Score: " + playerScore);
				cpuScoreLbl.setText("Score: " + cpuScore);
				if(cpuScore==0 || playerScore==0){
					long last=System.currentTimeMillis();
					while(System.currentTimeMillis()-last<1500){}
					main.gameLoop.cancel();
					Platform.runLater(new Runnable(){
						@Override
						public void run(){
							main.gameOver.conclude(main);
						}
					});
				}
				
			}
		},0,3000);
		
	}
	enum Moves{
		ROCK, PAPER, SCISSORS
	}
}

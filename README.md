**ABSTRACT**

This project aims to develop a hands-free interface that allows the players to control
PC games without any game controllers. In other words, it allows users to play games
just by moving their body as in playing against another live human. To achieve this,
the movement of user's body is recorded by a camera and then the recorded graphics
are processed by the PC.
This technique is herein implemented to simulate the famous children's game “Rock-
Paper-Scissors”. The game-play is described in detail in following sections. The key
is to differentiate between various shapes that the user turns his/her hand into.

**Software Details:**

Written in : Java 8

Image Processing : OpenCV 3.0

Voice Engine : FreeTTS 1.2

GUI Toolkit : JavaFX 8.0.6

IDE Used : Eclipse 4.5



**Developers:**

Ranjan Purbey (B. Tech Mechanical Engineering)

Amit Sharma (B. Tech Mechanical Engineering)

Ravi Kumar (B. Tech Mechanical Engineering)


**Algorithm :**

*1. Image Capture:*
The first step is to capture the image through a camera. Both, the integrated
webcam as well as USB cameras can be used for this.

*2. Grayscale:*
Secondly, the captured image is converted from RGB (actually BGR) color
space to Grayscale.

*3. Gaussian Blur:*
The grayscale image obtained in step 2 is then blurred so as to smoothen the
edges.

*4. Thresholding:*
The next step enables us to achieve a binary image by thresholding the gray
shades of our grayscale image to a certain brightness. Achieving a binary
image is necessary because the following operations cannot be applied on a
32-bit three channel (BGR) image.

*5. Contour Detection:*
The binary image developed in step 4 contains black (corresponding to dark
colors) and white (corresponding to light colors) regions. We assume the
player places his hand at optimum distance from the camera so that it covers
nearly 70-80 % of the total image area. Hence, we detect the largest white
region and draw a contour around it.

*6. Convex Hull & Convexity Defects:*
Thereafter, the contour from step 5 is used to draw a convex polygon
surrounding the player's hand. The number of convexity defects in this
polygon gives us an indication of number of fingers. That is the key in this
game. When the player shows a “Rock”, no fingers are drawn, but in case of
“Scissors” exactly the player draws exactly two fingers. More than two fingers
suggest a “Paper”

**Screenshot**
![screenshot](screenshots/1.png)
**Further Enhancements:**

• Currently a white glove is required to provide contrast to player's hand.
Using better background subtraction algorithm, this requirement can be
removed.

• A more customizable UI can be designed.

• The game can be made made more “hands-free” by providing voice
commands in the menu screen.

